//
//  Utilities.swift
//  DmitryTest
//
//  Created by Максуд on 27.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import Foundation

extension Data {
    func MD5() -> String {
        let digestLength = Int(CC_MD5_DIGEST_LENGTH)
        let md5Buffer = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: digestLength)
        let _ = withUnsafeBytes { (body: UnsafePointer<UInt8>) in
            CC_MD5(body, CC_LONG(count), md5Buffer)
        }
        
        var output = ""//String(capacity: Int(CC_MD5_DIGEST_LENGTH * 2))
        for i in 0..<digestLength {
            output = output.appendingFormat("%02x", md5Buffer[i])
        }
        return String(format: output)
    }
}
