//
//  PhotoCell.swift
//  DmitryTest
//
//  Created by Максуд on 27.11.2017.
//  Copyright © 2017 Максуд. All rights reserved.
//

import UIKit
import Photos

class PhotoCell: UICollectionViewCell {
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var hashLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var representedAssetIdentifier: String!
    
    @IBOutlet weak var progressLabel: UILabel!
    override func prepareForReuse() {
        previewImageView.image = nil
        hashLabel.text = nil
        nameLabel.text = nil
        progressLabel.text = nil
    }
    
    func configure(withImage image: UIImage?, info: [AnyHashable : Any]?){
        let nameUrl = info?["PHImageFileURLKey"] as? NSURL
        let imageDataRepresentation = UIImageJPEGRepresentation(image!, 1)
        
        self.previewImageView.image = image
        if let url = nameUrl{
            self.nameLabel.text = url.lastPathComponent
        }
        self.sizeLabel.text = "Bytes: \(imageDataRepresentation?.count ?? 0)"
        self.hashLabel.text = imageDataRepresentation?.MD5()
        self.progressLabel.text = ((info?[PHImageResultIsInCloudKey] as? NSNumber)?.boolValue == true) ? "0%" : "100%"
    }
}
